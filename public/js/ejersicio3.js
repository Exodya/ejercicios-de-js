
function case1() {
    document.getElementById("whatIsMyName").addEventListener("click", whatIsMyName);

    let input1 = document.getElementById("name");

    input1.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("whatIsMyName").click();
        }
    });

    function whatIsMyName() {
        let name = document.getElementById("name").value;
        document.getElementById('getName').innerHTML = 'Hello ' + name + ' a continuacion deberas ingresar 2 numero para sumarlos, Ok?';

        let show = document.getElementById("show");
        show.style.display = "none";

        let okay = document.getElementById("okay");
        okay.style.display = "block";
    }

}

case1();

function case2() {
    document.getElementById("Ok").addEventListener("click", Ok);

    function Ok() {
        let input1 = document.getElementById("input1");
        input1.style.display = "block";

        let okay = document.getElementById("okay");
        okay.style.display = "none";
    }
}

case2();

function case3() {
    document.getElementById("postNum1").addEventListener("click", postNum1);

    let input1 = document.getElementById("input1");

    input1.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("postNum1").click();
        }
    });

    function postNum1() {
        let input2 = document.getElementById("input2");
        input2.style.display = "block";
        let input1 = document.getElementById("input1");
        input1.style.display = "none";
    }
}

case3();

function case4() {
    document.getElementById("postNum2").addEventListener("click", postNum2);

    let input1 = document.getElementById("input2");

    input1.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById("postNum2").click();
        }
    });

    function postNum2() {
        let post = document.getElementById("post");
        post.style.display = "block";

        let input2 = document.getElementById("input2");
        input2.style.display = "none";

        let name = document.getElementById("name").value;
        document.getElementById('postName').innerHTML = 'muy bien ' + name + ' Ahora deberas sumar los numeros';
    }
}

case4();

function case5() {
    document.getElementById("suma").addEventListener("click", suma);

    function suma() {
        let num1 = document.getElementById("num1").value;
        let num2 = document.getElementById("num2").value;
        let name = document.getElementById("name").value;

        document.getElementById('result').innerHTML = name + ' the result is ' + Number(parseInt(num1) + parseInt(num2));

        let post = document.getElementById("post");
        post.style.display = "none";
        let refresh = document.getElementById("refresh");
        refresh.style.display = "block";
    }
}

case5();

function case6() {
    document.getElementById("refresh").addEventListener("click", refresh);

    function refresh() {
        location.reload();
    }
}

case6();
