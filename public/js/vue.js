const app = new Vue({
    el: '#app',
    data: {
      
        frutas: [
            {
                id: 1,
                name: 'peras',
                estado: false,
                cantidad: 5
            },
            {
                id: 2,
                name: 'sandias',
                estado: false,
                cantidad: 0
            },
            {
                id: 3,
                name: 'uvas',
                estado: false,
                cantidad: 8
            }
        ],
        newtype: 'kiwi',
        newfrut: '',
        total: 0,
    },
    methods:{
        addtype(){
            this.frutas.push({
                name: this.newtype, cantidad: 0
            }),
            this.newtype =''
        },
         addFrut (){
             this.frutas.push({
                name: this.newfrut, cantidad: 0
             });
             this.newfrut =''
         }
    },
    computed: {
        sumarFruts(){
            this.total = 0;
            for(fruta of this.frutas){
               this.total = this.total + fruta.cantidad; 
            }
            return this.total;
        }
    }

})