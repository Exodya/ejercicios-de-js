
let myNodelist = document.getElementsByTagName("LI");
for (let i = 0; i < myNodelist.length; i++) {
  let span = document.createElement("SPAN");
  span.className = "close px-4 py-1 bg-red-600 text-white fas fa-trash-alt rounded";
  myNodelist[i].appendChild(span);
}


// Click on a close button to hide the current list item
let close = document.getElementsByClassName("close");
for (let i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    let div = this.parentElement;
    div.style.display = "none";
  }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('text-gray-600');
    ev.target.classList.toggle('bg-opacity-75');
    ev.target.classList.toggle('line-through');
  
  }
}, false);

// Create a new list item when clicking on the "Add" button

document.getElementById("newElement").addEventListener("click", newElement);

function newElement() {
    var li = document.createElement("li");

    li.className = "p-2 px-10 flex justify-between"

    var inputValue = document.getElementById("myInput").value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue === '') {
      alert("You must write something!");
    } else {
      document.getElementById("myUL").appendChild(li);
    }
    document.getElementById("myInput").value = "";
  

    var span = document.createElement("SPAN");
 
    span.className = "close px-4 py-1 bg-red-600 text-white fas fa-trash-alt rounded";

    li.appendChild(span);
  
    for (i = 0; i < close.length; i++) {
      close[i].onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
      }
    }
  }

  const form = document.getElementById('form');

  form.addEventListener('submit', newElement);
// The length property sets or returns the number of elements in an array.

