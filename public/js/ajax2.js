console.log('activo :)');

document.getElementById('btn').addEventListener('click', traerdatos);

function traerdatos() {
    console.log('lopez');

    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'catalogo.json', true);

    xhttp.send();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
            let datos = JSON.parse(this.responseText);
           // console.log(datos);
           let res = document.getElementById('res');
           res.innerHTML = '';

            for(let item of datos){
              //  console.log(item.artista);
              res.innerHTML += `
              <tr class="bg-white">
              <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                ${item.titulo}
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
              ${item.artista}
              </td>
            </tr>
              `
            }
        }
    }
}