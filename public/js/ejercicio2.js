


document.write(' <form action="" method="post" class="form_contact" id="Form"><h2>DATOS DE CONTACTO</h2><div class=""><label for="dni_number">Numero de identificación</label><br><input type="text" name="id" id="id" value="1" /><br><br><label for="nombreyapellido">Nombre y Apellido</label><br><input type="text" name="name" value="Pedro Pérez" /><br><br><label for="contact_phone">Celular</label><br><input type="tel" name="contact" value="633555555" /><br><br><label for="email_address">Correo Electrónico</label><br><input type="email" name="email" value="uncorreo@mail.com" /><br><br><button type="submit" value="Enviar" id="btnSend">Enviar</button></div></form>')

document.addEventListener("DOMContentLoaded", function (e) {

    var miForm = document.getElementById('Form');
    miForm.onsubmit = function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        var jsonData = {};
        for (var [k, v] of formData) {
            jsonData[k] = v;
        }
        var jsonStr = JSON.stringify(jsonData);
        console.log(jsonData);
        document.body.innerHTML = jsonStr;

    }

});

  // The submit event fires when the user submits a form.

  //addEventListener()funciona agregando una función o un objeto 
  //que se implementa EventListenera la lista de detectores de eventos para
  // el tipo de evento especificado en el EventTargetque se llama.


  // The Event interface's preventDefault() method tells the user
  // agent that if the event does not get explicitly handled, 
  //its default action should not be taken as it normally would be. 
  //The event continues to propagate as usual, unless one of its event 
  //listeners calls stopPropagation() or stopImmediatePropagation(), 
  //either of which terminates propagation at once.

  //La FormDatainterfaz proporciona una forma de construir fácilmente un 
  //conjunto de pares clave / valor que representan campos de formulario y sus valores,
  // que luego se pueden enviar fácilmente mediante el XMLHttpRequest.send()método.
  // Utiliza el mismo formato que usaría un formulario si se configurara el tipo de
  // codificación "multipart/form-data".